<?php

/**
 *  Default Price
 * 
 */
function get_default_pachakge_price($package = 'p4')
{
    $package_price = array(
        'p1' => 132,
        'p2' => 167,
        'p3' => 184,
        'p4' => 254,
        'p5' => 323,
        'p6' => 392,
        'p7' => 430,
        'p8' => 456
    );
    return $package_price[$package];
}

function get_package_weight_range()
{
    return array(
        'p1' => 'upto 2.5kg',
        'p2' => '2.5kg - 3.5kg',
        'p3' => '3.5kg - 4kg',
        'p4' => '4kg - 6kg',
        'p5' => '6kg - 8kg',
        'p6' => '8kg - 10kg',
        'p7' => '10kg - 13kg',
        'p8' => '13kg - 15kg'
    );
}

/**
 *  Add Custom Fields On Cart Page, Checkout
 *  Add a From Country and a State Field to be selected
 *  Calculate Trhough AJAX using following hooks
 */

add_action('woocommerce_cart_totals_after_shipping', 'sdn_shipping_country_weight', 20);
add_action('woocommerce_review_order_after_shipping', 'sdn_shipping_country_weight_checkout', 20);
function sdn_shipping_country_weight()
{
    $countries = WC()->countries->get_shipping_countries();
    $from_country = WC()->session->get('from_country') ? WC()->session->get('from_country') : 'BD';

    $to_country = WC()->customer->get_shipping_country();
    $from_country_list = sdn_get_existing_countries('from');
    $dest_country_list = sdn_get_existing_countries('dest', $countries[$from_country]);


?>
    </tr>
    <tr class="shipping info shipping_from_country">
        <th>
            Shipping from
        </th>
        <td data-title="Delivery info">
            <p class="form-row form-row-wide" id="calc_shipping_country_field">
                <select name="calc_from_country" id="calc_from_country" class="country_to_state country_select cal_from_address" rel="calc_from_state">
                    <option value="0"><?php esc_html_e('Select a country', 'woocommerce'); ?></option>
                    <?php
                    foreach (WC()->countries->get_shipping_countries() as $key => $value) {
                        if (in_array($value, $from_country_list)) {
                            echo '<option value="' . esc_attr($key) . '"' . selected($from_country, esc_attr($key), false) . '>' . esc_html($value) . '</option>';
                        }
                    }
                    ?>
                </select>
            </p>
        </td>
    </tr>
    <tr class="shipping info shipping_dest_country">
        <th>
            Shipping to
        </th>
        <td data-title="Delivery info">
            <p class="form-row form-row-wide" id="calc_shipping_country_field">
                <select name="calc_dest_country" id="calc_dest_country" class="country_to_state country_select cal_dest_address" rel="calc_dest_state">
                    <option value="0"><?php esc_html_e('Select a country', 'woocommerce'); ?></option>
                    <?php
                    foreach (WC()->countries->get_shipping_countries() as $key => $value) {
                        if (in_array($value, $dest_country_list)) {
                            echo '<option value="' . esc_attr($key) . '"' . selected($to_country, esc_attr($key), false) . '>' . esc_html($value) . '</option>';
                        }
                    }
                    ?>
                </select>
            </p>
        </td>
    </tr>
    <tr class="shipping info shipping_weight">
        <th>
            Weight
        </th>
        <td data-title="Delivery info">
            <p class="form-row form-row-wide" id="calc_shipping_country_field">
                <select name="calc_shipping_weight" id="calc_shipping_weight" class="calc_shipping_weight">
                    <option value="default"><?php esc_html_e('Select weight range', 'woocommerce'); ?></option>
                    <?php
                    $package_name = WC()->session->get('shipping_weight') ? WC()->session->get('shipping_weight') : 'p4';
                    foreach (get_package_weight_range() as $key => $value) {
                        echo '<option value="' . esc_attr($key) . '"' . selected($package_name, esc_attr($key), false) . '>' . esc_html($value) . '</option>';
                    }
                    ?>
                </select>
            </p>
            <p class="update_shipping_cost_holder"><button type="submit" name="update_shipping_cost" class="button">
                    Update
                </button>
            </p>
        </td>
    <?php
}

function sdn_shipping_country_weight_checkout()
{
    $countries = WC()->countries->get_shipping_countries();
    $from_country = WC()->session->get('from_country') ? WC()->session->get('from_country') : 'BD';
    $to_country = WC()->customer->get_shipping_country() ? WC()->customer->get_shipping_country() : '';

    $packages = get_package_weight_range();


    ?>
    </tr>
    <tr class="shipping info shipping_from_country">
        <th>
            Shipping from
        </th>
        <td data-title="Delivery info">
            <p class="form-row form-row-wide" id="calc_shipping_country_field">
                <?php echo $countries[$from_country]; ?>
            </p>
        </td>
    </tr>
    <tr class="shipping info shipping_dest_country">
        <th>
            Shipping to
        </th>
        <td data-title="Delivery info">
            <p class="form-row form-row-wide" id="calc_shipping_country_field">
                <?php echo $countries[$to_country]; ?>
            </p>
        </td>
    </tr>
    <tr class="shipping info shipping_weight">
        <th>
            Weight
        </th>
        <td data-title="Delivery info">
            <p class="form-row form-row-wide" id="calc_shipping_country_field">
                <?php
                $package_name = WC()->session->get('shipping_weight') ? WC()->session->get('shipping_weight') : 'p4';
                echo $packages[$package_name];
                ?>
            </p>
        </td>
    <?php
}

/**
 *  Add fees as a custom total shipping cost
 *  Add Discounts on total shipping cost - default is 60%
 */
add_action('woocommerce_cart_calculate_fees', 'woocommerce_custom_surcharge');

function woocommerce_custom_surcharge()
{
    if (is_admin() && !defined('DOING_AJAX')) return;
    global $woocommerce;
    $shipping_discount = sdn_calculate_shipping_discount();
    // Check Price
    $shipcharge = sdn_get_shipping_charge();

    if ($shipcharge > 0) $woocommerce->cart->add_fee('Shipping Cost', $shipcharge, false);
    if ($shipcharge > 0) $woocommerce->cart->add_fee('Shipping Discount (' . $shipping_discount . '%)', - (floor($shipcharge * ($shipping_discount / 100))), false);
}

/**
 *  Add fees as a custom Shipping Charge
 *  Default charge set to 10 
 *  Extra shop charge will be 3
 */
add_action('woocommerce_cart_calculate_fees', 'woocommerce_custom_shopping_charge');

function woocommerce_custom_shopping_charge()
{
    if (is_admin() && !defined('DOING_AJAX')) return;
    global $woocommerce;
    $shop_links = array();
    $service_charge = get_option('sdn_service_charge', 10);
    $service_charge_extra = get_option('sdn_service_charge_rest', 3);

    $total_service_charge = 0;

    foreach ($woocommerce->cart->get_cart() as $key => $item) {
        if (isset($item['wcpa_data']) && !empty($item['wcpa_data'])) {
            foreach ($item['wcpa_data'] as $addon) {
                if (isset($addon['name'])  && $addon['name'] == 'product-link') {
                    if (isset($addon['value']) && !empty($addon['value'])) {
                        $parseUrl = parse_url($addon['value']);
                        if (!in_array($parseUrl['host'], $shop_links)) {
                            array_push($shop_links, $parseUrl['host']);
                            if (count($shop_links) > 1) {
                                $total_service_charge += $service_charge_extra;
                            } else {
                                $total_service_charge += $service_charge;
                            }
                        }
                    }
                }
            }
        }
    }

    $service_charge_title = "Service Charge ";
    $total_shops = count($shop_links);
    if ($total_shops > 1) {
        $service_charge_title .= "(" . $total_shops . " shops)";
    } else if ($total_shops > 0) {
        $service_charge_title .= "(" . $total_shops . " shop)";
    }
    // Check Price

    if ($total_service_charge > 0) $woocommerce->cart->add_fee($service_charge_title, $total_service_charge, false);
}

/**
 *  Add Fees as Alter Charge
 *  Default alter charge is 8 per item
 */
add_action('woocommerce_cart_calculate_fees', 'woocommerce_custom_alter_charge');

function woocommerce_custom_alter_charge()
{
    if (is_admin() && !defined('DOING_AJAX')) return;
    global $woocommerce;
    $alter_charge = get_option('sdn_service_alter_fee', 8);
    $total_alter_charge = 0;

    foreach ($woocommerce->cart->get_cart() as $key => $item) {
        if (isset($item['wcpa_data']) && !empty($item['wcpa_data'])) {
            foreach ($item['wcpa_data'] as $addon) {
                if (isset($addon['name'])  && $addon['name'] == 'alter-product') {
                    if (isset($addon['value']) && !empty($addon['value']) && $addon['value'][0] == 'yes') {
                        $total_alter_charge += $alter_charge;
                    }
                }
            }
        }
    }

    // Check Price

    if ($total_alter_charge > 0) $woocommerce->cart->add_fee("Alter Fee", $total_alter_charge, false);
}

/**
 *  Shipping Charge Calcualtor function
 *  sdn_get_shipping_charge
 *  Pass Variable pachange name
 *  default is p4
 */
if (!function_exists('sdn_get_shipping_charge')) {
    function sdn_get_shipping_charge()
    {
        global $woocommerce, $wpdb;
        $shipping_charge = get_default_pachakge_price();

        $shipping_country_code = WC()->customer->get_shipping_country() ? WC()->customer->get_shipping_country() : 'BD';
        $from_country_code = $woocommerce->session->get('from_country') ? $woocommerce->session->get('from_country') : 'BD';
        $package_name = $woocommerce->session->get('shipping_weight') ? $woocommerce->session->get('shipping_weight') : 'p4';
        $countries = $woocommerce->countries->get_shipping_countries();
        if ($shipping_country_code && $from_country_code) {
            $shipping_country = $countries[$shipping_country_code];
            $from_country = $countries[$from_country_code];
            $tablename = $wpdb->prefix . "dhl_price_table";
            $cntSQL = "SELECT * FROM {$tablename} 
        WHERE from_country='" . $from_country . "'
        AND dest_country='" . $shipping_country . "'
        AND name='" . $package_name . "'";
            $record = $wpdb->get_row($cntSQL);
            if ($record) {
                $shipping_charge = $record->shipping_cost;
            }
        }
        return $shipping_charge;
    }
}

/**
 *  Get Existing Countries From the database
 *  sdn_get_existing_countries
 */
if (!function_exists('sdn_get_existing_countries')) {
    function sdn_get_existing_countries($type = 'dest', $from_country = 'Bangladesh')
    {
        global $wpdb;
        $country_list = array();
        $tablename = $wpdb->prefix . "dhl_price_table";
        if ($type == 'from') {
            $cntSQL = "SELECT `from_country` as `country` FROM {$tablename} GROUP BY `from_country`";
        } else if ($type == 'dest') {
            $cntSQL = "SELECT `dest_country` as `country` FROM {$tablename} WHERE `from_country` = '{$from_country}' GROUP BY `dest_country`";
        } else {
            $cntSQL = "SELECT `dest_country` as `country` FROM {$tablename} GROUP BY `dest_country`";
        }

        $record = $wpdb->get_results($cntSQL);
        if ($record && count($record) > 0) {
            foreach ($record as $country) {
                $country_list[] = $country->country;
            }
        }
        return $country_list;
    }
}

/**
 *  Ajax call when changing From Country
 *  Updaing from address
 */
add_action('wp_footer', 'cart_from_address_update');
function cart_from_address_update()
{
    ?>
        <script type="text/javascript">
            jQuery('.woocommerce').on('click', '[name="update_shipping_cost"]', function() {
                var from_country = jQuery('#calc_from_country').val()
                var dest_country = jQuery('#calc_dest_country').val()
                var shipping_weight = jQuery('#calc_shipping_weight').val()
                console.log(dest_country)
                jQuery.ajax({
                    type: "post",
                    dataType: "json",
                    url: woocommerce_params.ajax_url,
                    data: {
                        action: "update_shipping_variables",
                        from_country: from_country,
                        dest_country: dest_country,
                        shipping_weight: shipping_weight
                    },
                    success: function(response) {
                        if (response.type == "success") {
                            /* jQuery("[name='update_cart']").removeAttr('disabled');
                            jQuery("[name='update_cart']").trigger("click"); */
                            location.reload();
                        } else {
                            console.log(false)
                        }
                    },
                    error: function(text, error) {
                        console.log(error)
                    }
                })
            })

            jQuery('.woocommerce').on('change', '#calc_shipping_weight', function() {
                var shipping_weight = jQuery(this).val()
                jQuery.ajax({
                    type: "post",
                    dataType: "json",
                    url: woocommerce_params.ajax_url,
                    data: {
                        action: "on_change_shipping_weight",
                        shipping_weight: shipping_weight,
                        /* from_state: from_state, */
                    },
                    success: function(response) {
                        if (response.type == "success") {
                            //jQuery('[name="calc_shipping"]').trigger('click')
                            jQuery(document.body).trigger('update_checkout')
                        } else {
                            console.log(false)
                        }
                    },
                    error: function(text, error) {
                        console.log(error)
                    }
                })
            })

            /* jQuery('.woocommerce').on('change', '.cal_dest_address', function() {
                jQuery('#calc_shipping_country').val(jQuery(this).val()).trigger('change')
                //jQuery('[name="calc_shipping"]').trigger('click')
                jQuery(document.body).trigger('update_checkout')
            }) */

            jQuery('.woocommerce').on('change', '.cal_from_address', function() {
                var from_country = jQuery('#calc_from_country').val()
                /* var from_state = jQuery('#calc_from_state').val() */
                jQuery.ajax({
                    type: "post",
                    dataType: "json",
                    url: woocommerce_params.ajax_url,
                    data: {
                        action: "on_change_from_country",
                        from_country: from_country,
                        /* from_state: from_state, */
                    },
                    success: function(response) {
                        if (response.type) {
                            //jQuery('[name="calc_shipping"]').trigger('click')
                            jQuery(document.body).trigger('update_checkout');
                            jQuery('#calc_dest_country').html(response.countries).select2();
                        } else {
                            console.log(false)
                        }
                    },
                    error: function(text, error) {
                        console.log(error)
                    }
                })
            })
        </script>
    <?php
}
/**
 * Ajax Calls
 */
add_action("wp_ajax_on_change_from_country", "on_change_from_country");
add_action("wp_ajax_nopriv_on_change_from_country", "on_change_from_country");

add_action("wp_ajax_on_change_shipping_weight", "on_change_shipping_weight");
add_action("wp_ajax_nopriv_on_change_shipping_weight", "on_change_shipping_weight");

add_action("wp_ajax_update_shipping_variables", "update_shipping_variables");
add_action("wp_ajax_nopriv_update_shipping_variables", "update_shipping_variables");

function update_shipping_variables()
{
    if (!session_id()) session_start();

    WC()->session->set('from_country', trim($_REQUEST['from_country']));
    WC()->session->set('shipping_weight', trim($_REQUEST['shipping_weight']));
    //WC()->customer->set_shipping_country(trim($_REQUEST['dest_country']));

    WC()->customer->set_country(trim($_REQUEST['dest_country']));
    WC()->customer->set_billing_country(trim($_REQUEST['dest_country'])); // Updated HERE Too
    WC()->customer->set_shipping_country(trim($_REQUEST['dest_country']));

    $result['type'] = 'success';
    echo json_encode($result);

    die();
}

/**
 *  Ajax Callback Function
 *  on_change_from_country
 */

function on_change_from_country()
{
    if (!session_id()) session_start();

    $result['countries'] = array();
    $result['type'] = false;

    if (isset($_REQUEST['from_country']) && !empty($_REQUEST['from_country'])) {
        WC()->session->set('from_country', trim($_REQUEST['from_country']));
        $countries = WC()->countries->get_shipping_countries();

        $dest_countries = sdn_get_existing_countries('dest', $countries[$_REQUEST['from_country']]);
        /* $dest_country_list = array_filter(
            $countries,
            fn ($country) => in_array($country, $dest_countries)
        ); */
        $dest_country = WC()->customer->get_shipping_country();
        $dest_country_list = '<option value="">Select a country</option>';
        foreach ($countries as $key => $country) {
            if (in_array($country, $dest_countries))
                $dest_country_list .= '<option value="' . esc_attr($key) . '"' . selected($dest_country, esc_attr($key), false) . '>' . esc_html($country) . '</option>';
        }

        $result['type'] = true;
        $result['countries'] = $dest_country_list;
    }

    echo json_encode($result);
    die();
}

/**
 *  Ajax Callback Function
 *  on_change_shipping_weight
 */

function on_change_shipping_weight()
{
    if (!session_id()) session_start();

    WC()->session->set('shipping_weight', trim($_REQUEST['shipping_weight']));

    $result['type'] = 'success';
    echo json_encode($result);

    die();
}

/**
 * Function sdn_calculate_shipping_discount
 */

function sdn_calculate_shipping_discount($shipping_discount = 20)
{
    $from_country = WC()->session->get('from_country') ? WC()->session->get('from_country') : false;
    $shipping_discount = get_option('sdn_service_discount_all', $shipping_discount);
    if ($from_country && !empty($from_country)) {
        switch ($from_country) {
            case 'IN':
                $shipping_discount = get_option('sdn_service_discount_india', $shipping_discount);
                break;
            case 'CA':
                $shipping_discount = get_option('sdn_service_discount_canada', $shipping_discount);
                break;
            case 'PK':
                $shipping_discount = get_option('sdn_service_discount_pakistan', $shipping_discount);
                break;
            case 'BD':
                $shipping_discount = get_option('sdn_service_discount_bangladesh', $shipping_discount);
                break;
            case 'TR':
                $shipping_discount = get_option('sdn_service_discount_turkey', $shipping_discount);
                break;
        }
    }

    return $shipping_discount;
}
