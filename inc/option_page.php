<?php

add_action('admin_menu', 'sdn_shipping_settings_page');

function sdn_shipping_settings_page()
{

    add_options_page(
        'Shopping Day Night Settings', // page <title>Title</title>
        'Shopping Day Night', // menu link text
        'manage_options', // capability to access the page
        'sdn-settings', // page URL slug
        'shopping_day_night_settings', // callback function with content
        9 // priority
    );
}

function shopping_day_night_settings()
{
    // Save data on submit

    if (isset($_POST['sdn_service_charge']) && !empty($_POST['sdn_service_charge'])) update_option('sdn_service_charge', $_POST['sdn_service_charge']);
    if (isset($_POST['sdn_service_charge_rest']) && !empty($_POST['sdn_service_charge_rest'])) update_option('sdn_service_charge_rest', $_POST['sdn_service_charge_rest']);
    if (isset($_POST['sdn_service_alter_fee']) && !empty($_POST['sdn_service_alter_fee'])) update_option('sdn_service_alter_fee', $_POST['sdn_service_alter_fee']);
    if (isset($_POST['sdn_service_discount_all']) && !empty($_POST['sdn_service_discount_all'])) update_option('sdn_service_discount_all', $_POST['sdn_service_discount_all']);
    if (isset($_POST['sdn_service_discount_canada']) && !empty($_POST['sdn_service_discount_canada'])) update_option('sdn_service_discount_canada', $_POST['sdn_service_discount_canada']);
    if (isset($_POST['sdn_service_discount_bangladesh']) && !empty($_POST['sdn_service_discount_bangladesh'])) update_option('sdn_service_discount_bangladesh', $_POST['sdn_service_discount_bangladesh']);
    if (isset($_POST['sdn_service_discount_india']) && !empty($_POST['sdn_service_discount_india'])) update_option('sdn_service_discount_india', $_POST['sdn_service_discount_india']);
    if (isset($_POST['sdn_service_discount_turkey']) && !empty($_POST['sdn_service_discount_turkey'])) update_option('sdn_service_discount_turkey', $_POST['sdn_service_discount_turkey']);
    if (isset($_POST['sdn_service_discount_pakistan']) && !empty($_POST['sdn_service_discount_pakistan'])) update_option('sdn_service_discount_pakistan', $_POST['sdn_service_discount_pakistan']);

?>
    <!-- Form -->
    <div>
        <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
            <h1>Shopping Day Night Settings</h1>
            <p>In this page you can update your Shipping options</p>
            <table>
                <tr valign="top" align="left">
                    <th scope="row"><label for="sdn_service_charge">Service Charge (in USD)</label></th>
                    <td><input type="number" id="sdn_service_charge" name="sdn_service_charge" min="0" max="100" value="<?php echo get_option('sdn_service_charge'); ?>" /></td>
                </tr>
                <tr valign="top" align="left">
                    <th scope="row"><label for="sdn_service_charge_rest">Service Charge - more than 1 shop (in USD)</label></th>
                    <td><input type="number" id="sdn_service_charge_rest" name="sdn_service_charge_rest" min="0" max="100" value="<?php echo get_option('sdn_service_charge_rest'); ?>" /></td>
                </tr>
                <tr valign="top" align="left">
                    <th scope="row"><label for="sdn_service_alter_fee">Alter Fees (in USD)</label></th>
                    <td><input type="number" id="sdn_service_alter_fee" name="sdn_service_alter_fee" min="0" max="100" value="<?php echo get_option('sdn_service_alter_fee'); ?>" /></td>
                </tr>
                <tr valign="top" align="left">
                    <th scope="row"><label for="sdn_service_discount_all">Discount - all countries (in %)</label></th>
                    <td><input type="number" id="sdn_service_discount_all" name="sdn_service_discount_all" min="0" max="100" value="<?php echo get_option('sdn_service_discount_all'); ?>" /></td>
                </tr>
                <tr valign="top" align="left">
                    <th scope="row"><label for="sdn_service_discount_canada">Discount - from Canada (in %)</label></th>
                    <td><input type="number" id="sdn_service_discount_canada" name="sdn_service_discount_canada" min="0" max="100" value="<?php echo get_option('sdn_service_discount_canada'); ?>" /></td>
                </tr>
                <tr valign="top" align="left">
                    <th scope="row"><label for="sdn_service_discount_india">Discount - from India (in %)</label></th>
                    <td><input type="number" id="sdn_service_discount_india" name="sdn_service_discount_india" min="0" max="100" value="<?php echo get_option('sdn_service_discount_india'); ?>" /></td>
                </tr>
                <tr valign="top" align="left">
                    <th scope="row"><label for="sdn_service_discount_bangladesh">Discount - from Bangladesh (in %)</label></th>
                    <td><input type="number" id="sdn_service_discount_bangladesh" name="sdn_service_discount_bangladesh" min="0" max="100" value="<?php echo get_option('sdn_service_discount_bangladesh'); ?>" /></td>
                </tr>
                <tr valign="top" align="left">
                    <th scope="row"><label for="sdn_service_discount_turkey">Discount - from Turkey (in %)</label></th>
                    <td><input type="number" id="sdn_service_discount_turkey" name="sdn_service_discount_turkey" min="0" max="100" value="<?php echo get_option('sdn_service_discount_turkey'); ?>" /></td>
                </tr>
                <tr valign="top" align="left">
                    <th scope="row"><label for="sdn_service_discount_pakistan">Discount - from Pakistan (in %)</label></th>
                    <td><input type="number" id="sdn_service_discount_pakistan" name="sdn_service_discount_pakistan" min="0" max="100" value="<?php echo get_option('sdn_service_discount_pakistan'); ?>" /></td>
                </tr>
            </table>
            <?php submit_button(); ?>
        </form>
    </div>
    <!-- Form -->
<?php
}
