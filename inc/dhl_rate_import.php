<?php

add_action('admin_menu', 'sdn_dhl_rate_import_options_page');

function sdn_dhl_rate_import_options_page()
{

    add_options_page(
        'DHL Rate Import', // page <title>Title</title>
        'DHL Rate Import', // menu link text
        'manage_options', // capability to access the page
        'dhl-rate-import', // page URL slug
        'shopping_day_night_dhl_import', // callback function with content
        10 // priority
    );
}

function shopping_day_night_dhl_import()
{
    // Import CSV
    if (isset($_POST['shipping_import'])) {
        // File extension
        $extension = pathinfo($_FILES['shipping_file']['name'], PATHINFO_EXTENSION);

        // If file extension is 'csv'
        if (!empty($_FILES['shipping_file']['name']) && $extension == 'csv') {
            // Open file in read mode
            $csvFile = fopen($_FILES['shipping_file']['tmp_name'], 'r');
            save_dhl_shipping_rate($csvFile);
        }
    }
?>
    <h1>DHL Rate Import</h1>
    <!-- Form -->
    <form method='post' action='<?= $_SERVER['REQUEST_URI']; ?>' enctype='multipart/form-data'>
        <input type="file" name="shipping_file" accept=".csv">
        <input type="submit" name="shipping_import" value="Import">
    </form>
    <!-- Form -->
<?php
}


function save_dhl_shipping_rate($csvFile)
{
    $new_price = array();
    $packages = array(
        'p1' => 2.5,
        'p2' => 3.5,
        'p3' => 4,
        'p4' => 6,
        'p5' => 8,
        'p6' => 10,
        'p7' => 13,
        'p8' => 15
    );

    while (($data = fgetcsv($csvFile)) !== FALSE) {
        //process
        if ($data[0] == 'country') {
            $base_country = $data[1];
        }
        foreach ($data as $key => $c) {
            if ($key == 1) {
            }
            if ($key != 0 && $key != 1) {
                $new_price[$base_country][$key][$data[0]] = $c;
            }
        }
    }

    $totalInserted = 0;

    foreach ($new_price as $key => $base) {
        foreach ($base as $b_key => $price) {
            //echo $key . ' - ' . $b_key . '</br>';
            foreach ($price as $k => $p) {
                if ($key !== $price['country'] && $k !== 'country') {
                    //echo $key . '-' . $price['country'] . '-' . $k . '-' . $p . '</br>';
                    $base_country = trim($base_country);
                    $dest_country = trim($price['country']);
                    $package_name = trim($k);
                    $package_weight = $packages[$package_name];
                    $package_price = trim($p);

                    if (sdn_insert_dhl_pricing_by_country($base_country, $dest_country, $package_name, $package_price, $package_weight)) $totalInserted++;
                    if (sdn_insert_dhl_pricing_by_country($dest_country, $base_country, $package_name, $package_price, $package_weight)) $totalInserted++;
                    /// Function
                }
            }
        }
    }

    echo "<h3 style='color: green;'>Total record Inserted : " . $totalInserted . "</h3>";
}


function sdn_insert_dhl_pricing_by_country($base_country = '', $dest_country = '', $package_name = '', $package_price = '', $package_weight = '')
{
    $inserted = false;
    if (!empty($base_country) && !empty($dest_country) && !empty($package_name) && !empty($package_price) && !empty($package_weight)) {
        global $wpdb;
        // Table name
        $tablename = $wpdb->prefix . "dhl_price_table";

        // Check record already exists or not
        $cntSQL = "SELECT `ID`, `shipping_cost` FROM {$tablename} 
                    WHERE `from_country`='" . $base_country . "'
                    AND `dest_country`='" . $dest_country . "'
                    AND `name`='" . $package_name . "'";
        $record = $wpdb->get_results($cntSQL, OBJECT);

        if (count($record) > 0) {
            $record_id = $record[0]->ID;
            $shipping_price = $record[0]->shipping_cost;
            // Update Record
            if ($shipping_price !=  $package_price) {
                $data = $wpdb->update(
                    $tablename,
                    array(
                        'name' => $package_name,
                        'weight' => $package_weight,
                        'shipping_cost' => $package_price
                    ),
                    array(
                        'ID' => $record_id
                    )
                );
                if ($data) $inserted = true;
            }
        } else {
            // Check if variable is empty or not
            if (!empty($base_country) && !empty($dest_country) && !empty($package_price)) {
                // Insert Record
                $wpdb->insert($tablename, array(
                    'from_country' => $base_country,
                    'dest_country' => $dest_country,
                    'name' => $package_name,
                    'weight' => $package_weight,
                    'shipping_cost' => $package_price
                ));

                if ($wpdb->insert_id > 0) {
                    $inserted = true;
                }
            }
        }
    }
    return $inserted;
}
