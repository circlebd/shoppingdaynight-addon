<?php
/*
Plugin Name: Shopping Day Night Addon
Plugin URI: https://www.linkedin.com/in/mahfuzul-alam/
Description: An addon to calculate cutom shipping charge
Author: Mahfuzul Alam
Version: 1.0.0
Author URI: https://www.linkedin.com/in/mahfuzul-alam/
License: GPLv2 or later
Text Domain: shoppingdaynight-addon
*/

if (!defined('ABSPATH')) exit;

/**
 * Check if woocommerce exists or not
 */
if (
    !in_array(
        'woocommerce/woocommerce.php',
        apply_filters('active_plugins', get_option('active_plugins'))
    )
) {
    exit;
}
/**
 * Define plugin base path
 */
define('SHOPPING_DN_BASE_PATH', plugin_dir_path(__FILE__));

/**
 * Create DHL Price Table On Active
 */
register_activation_hook(__file__, 'create_dhl_price_table');

/**
 * Include Files - Admin Option Page
 */
include_once(__DIR__ . '/inc/option_page.php');
include_once(__DIR__ . '/inc/dhl_rate_import.php');


/**
 * Include Files - frontend shipping calcualtion
 */
include_once(__DIR__ . '/inc/shipping_calculation.php');

/**
 * Helper Function - DHL price Table
 */
function create_dhl_price_table()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "dhl_price_table";
    $my_products_db_version = '1.0.0';
    $charset_collate = $wpdb->get_charset_collate();

    if ($wpdb->get_var("SHOW TABLES LIKE '{$table_name}'") != $table_name) {

        $sql = "CREATE TABLE $table_name (
                ID mediumint(9) NOT NULL AUTO_INCREMENT,
                `from_country` varchar(2) NOT NULL,
                `dest_country` varchar(2) NOT NULL,
                `name` varchar(30) NOT NULL,
                `weight` varchar(10) NOT NULL,
                `height` varchar(10) NOT NULL,
                `length` varchar(10) NOT NULL,
                `width` varchar(10) NOT NULL,
                `shipping_cost` varchar(10) NOT NULL,
                `description` text NOT NULL,
                PRIMARY KEY  (ID)
        ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
        add_option('my_db_version', $my_products_db_version);
    }
}
